# -*- coding: utf-8 -*-
{
    'name': 'Website Scag',
    'version': '1.0',
    'author': 'Zahra Ahmed',
    'sequence': 4,
    'category': 'Website',
    'summary': 'Website under construction',
    'description':
        """
        """,
    'depends': ['website', 'website_forum', 'website_crm'],
    'data': [
        'views/external.xml',
        'views/templates.xml',
    ],
}
